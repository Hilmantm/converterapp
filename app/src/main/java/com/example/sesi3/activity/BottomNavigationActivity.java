package com.example.sesi3.activity;

import android.os.Bundle;

import com.example.sesi3.R;
import com.example.sesi3.fragments.TimeFragment;
import com.example.sesi3.fragments.TemperatureFragment;
import com.example.sesi3.fragments.WeightFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.MenuItem;

public class BottomNavigationActivity extends AppCompatActivity {
    private Fragment fragment;
    private final String FRAGMENT_KEY = "myFragment";

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_temperature:
                    fragment = new TemperatureFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.main_container, fragment).commit();
                    return true;
                case R.id.navigation_weight:
                    fragment = new WeightFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.main_container, fragment).commit();
                    return true;
                case R.id.navigation_time:
                    fragment = new TimeFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.main_container, fragment).commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_navigation);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        if(savedInstanceState == null) {
            navView.setSelectedItemId(R.id.navigation_temperature);
        } else {
            fragment = getSupportFragmentManager().getFragment(savedInstanceState, FRAGMENT_KEY);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        getSupportFragmentManager().putFragment(outState, FRAGMENT_KEY, fragment);
    }
}
