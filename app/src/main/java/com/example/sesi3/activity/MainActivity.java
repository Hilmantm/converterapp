package com.example.sesi3.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import android.widget.TextView;

import com.example.sesi3.R;

public class MainActivity extends AppCompatActivity {

    EditText inputGram;
    Button btnKonversi;
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponents();
        konversi();
    }

    private void konversi() {
        btnKonversi.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                String getGram = inputGram.getText().toString();
                float gram = 0;
                if(!getGram.equals("")) {
                    gram = Integer.parseInt(getGram);
                }
                float res = gram / 1000;
                result.setText(res + " kg");
            }
        });
    }

    private void initComponents() {
        inputGram = findViewById(R.id.edtGram);
        btnKonversi = findViewById(R.id.btnKonversi);
        result = findViewById(R.id.hasil);
    }

}
