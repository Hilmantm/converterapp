package com.example.sesi3.fragments;


import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.sesi3.R;

import java.text.DecimalFormat;
import java.text.NumberFormat;


/**
 * A simple {@link Fragment} subclass.
 */
public class TemperatureFragment extends Fragment {

    private Spinner temperatureTarget;
    private EditText temperatureInput;
    private TextView kelvinConvertValue;
    private TextView celciusConvertValue;
    private TextView fahrenheitConvertValue;
    private TextView reamurConvertValue;

    private final String KELVIN = "kelvin";
    private final String CELCIUS = "celcius";
    private final String FAHRENHEIT = "fahrenheit";
    private final String REAMUR = "reamur";
    private final String BUNDLE_INPUT = "input_state";

    private int destinationTemp;

    public TemperatureFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_temperature, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initComponents(view);
        setValueTemperatureTarget();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState != null) {
            String inputState = savedInstanceState.getString(BUNDLE_INPUT);
            String kelvinState = savedInstanceState.getString(KELVIN);
            String celciusState = savedInstanceState.getString(CELCIUS);
            String fahrenheitState = savedInstanceState.getString(FAHRENHEIT);
            String reamurState = savedInstanceState.getString(REAMUR);

            temperatureInput.setText(inputState);
            kelvinConvertValue.setText(kelvinState);
            celciusConvertValue.setText(celciusState);
            fahrenheitConvertValue.setText(fahrenheitState);
            reamurConvertValue.setText(reamurState);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(BUNDLE_INPUT, temperatureInput.getText().toString());
        outState.putString(KELVIN, kelvinConvertValue.getText().toString());
        outState.putString(CELCIUS, celciusConvertValue.getText().toString());
        outState.putString(FAHRENHEIT, fahrenheitConvertValue.getText().toString());
        outState.putString(REAMUR, reamurConvertValue.getText().toString());
    }

    private void setValueTemperatureTarget() {
        temperatureTarget.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                destinationTemp = i;
                if(temperatureInput.length() != 0) {
                    int temp = Integer.parseInt(temperatureInput.getText().toString());
                    calculateTemperature(i, temp);
                } else {
                    resetValue();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                destinationTemp = 0;
            }
        });
    }

    private void initComponents(View view) {
        temperatureInput = view.findViewById(R.id.input_temperature);
        temperatureTarget = view.findViewById(R.id.temperature_target);
        kelvinConvertValue = view.findViewById(R.id.kelvin_convert_value);
        celciusConvertValue = view.findViewById(R.id.celcius_convert_value);
        fahrenheitConvertValue = view.findViewById(R.id.fahrenheit_convert_value);
        reamurConvertValue = view.findViewById(R.id.reamur_convert_value);

        temperatureInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() == 0) {
                    resetValue();
                } else {
                    int temp = Integer.parseInt(s.toString());
                    calculateTemperature(destinationTemp, temp);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void resetValue() {
        double reset = 0;
        kelvinConvertValue.setText(reset + "");
        celciusConvertValue.setText(reset + "");
        fahrenheitConvertValue.setText(reset + "");
        reamurConvertValue.setText(reset + "");
    }

    @SuppressLint("SetTextI18n")
    private void calculateTemperature(int position, int input) {
        double kelvin, celcius, fahrenheit, reamur;
        switch (position) {
            case 0 :
                kelvin = setDoubleFormat(calculateKelvin(input, KELVIN));
                break;
            case 1 :
                kelvin = setDoubleFormat(calculateKelvin(input, CELCIUS));
                break;
            case 2 :
                kelvin = setDoubleFormat(calculateKelvin(input, FAHRENHEIT));
                break;
            case 3 :
                kelvin = setDoubleFormat(calculateKelvin(input, REAMUR));
                break;
            default :
                kelvin = setDoubleFormat(calculateKelvin(input, KELVIN));
                break;
        }

        celcius = setDoubleFormat(calculateCelcius(kelvin));
        fahrenheit = setDoubleFormat(calculateFahrenheit(kelvin));
        reamur = setDoubleFormat(calculateReamur(kelvin));

        kelvinConvertValue.setText(kelvin + "");
        celciusConvertValue.setText(celcius + "");
        fahrenheitConvertValue.setText(fahrenheit + "");
        reamurConvertValue.setText(reamur + "");

    }

    private double calculateKelvin(int input, String type) {
        double res = 0;
        if(type.equals(KELVIN)) {
            res = input;
        } else {
            switch (type) {
                case CELCIUS :
                    res = input +  273.15;
                    break;
                case FAHRENHEIT :
                    res = (input - 32) * 0.56 + 273.15;
                    break;
                case REAMUR :
                    res = input / 0.8 + 273.15;
                    break;
            }
        }
        return res;
    }

    private double calculateCelcius(double kelvin) {
        return kelvin - 273.15;
    }

    private double calculateFahrenheit(double kelvin) {
        return kelvin * 1.8 - 459.67;
    }

    private double calculateReamur(double kelvin) {
        return (kelvin-273.15) * 0.8;
    }

    private double setDoubleFormat(double tes) {
        NumberFormat formatter = new DecimalFormat("##.##");
        return Double.valueOf(formatter.format(tes));
    }

}
