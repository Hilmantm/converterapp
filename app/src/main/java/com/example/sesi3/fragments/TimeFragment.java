package com.example.sesi3.fragments;


import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.sesi3.R;

import java.text.DecimalFormat;
import java.text.NumberFormat;


/**
 * A simple {@link Fragment} subclass.
 */
public class TimeFragment extends Fragment {

    private Spinner timeTarget;
    private EditText timeInput;
    private TextView jamConvertValue;
    private TextView menitConvertValue;
    private TextView detikConvertValue;

    private final String JAM = "jam";
    private final String MENIT = "menit";
    private final String DETIK = "detik";
    private final String BUNDLE_INPUT = "input_state";

    private int destinationTime;

    public TimeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_time, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initComponents(view);
        setTimeTarget();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState != null) {
            String inputTime = savedInstanceState.getString(BUNDLE_INPUT);
            String jam = savedInstanceState.getString(JAM);
            String menit = savedInstanceState.getString(MENIT);
            String detik = savedInstanceState.getString(DETIK);

            timeInput.setText(inputTime);
            jamConvertValue.setText(jam);
            menitConvertValue.setText(menit);
            detikConvertValue.setText(detik);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(BUNDLE_INPUT, timeInput.getText().toString());
        outState.putString(JAM, jamConvertValue.getText().toString());
        outState.putString(MENIT, menitConvertValue.getText().toString());
        outState.putString(DETIK, detikConvertValue.getText().toString());
    }

    private void setTimeTarget() {
        timeTarget.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                destinationTime = position;
                if(timeInput.length() != 0) {
                    int time = Integer.parseInt(timeInput.getText().toString());
                    calculateTime(position, time);
                } else {
                    resetValue();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initComponents(View view) {
        timeInput = view.findViewById(R.id.input_time);
        timeTarget = view.findViewById(R.id.time_target);
        jamConvertValue = view.findViewById(R.id.jam_convert_value);
        menitConvertValue = view.findViewById(R.id.menit_convert_value);
        detikConvertValue = view.findViewById(R.id.detik_convert_value);

        timeInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() == 0) {
                    resetValue();
                } else {
                    int time = Integer.parseInt(s.toString());
                    calculateTime(destinationTime, time);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void resetValue() {
        double reset = 0;
        jamConvertValue.setText(reset + "");
        menitConvertValue.setText(reset + "");
        detikConvertValue.setText(reset + "");
    }

    @SuppressLint("SetTextI18n")
    private void calculateTime(int position, int input) {
        double jam, menit, detik;
        switch (position) {
            case 0 :
                detik = setDoubleFormat(calculateSecond(input, JAM));
                break;
            case 1 :
                detik = setDoubleFormat(calculateSecond(input, MENIT));
                break;
            case 2 :
                detik = setDoubleFormat(calculateSecond(input, DETIK));
                break;
            default :
                detik = setDoubleFormat(calculateSecond(input, JAM));
                break;
        }

        jam = setDoubleFormat(calculateHours(detik));
        menit = setDoubleFormat(calculateMinute(detik));

        jamConvertValue.setText(jam + "");
        menitConvertValue.setText(menit + "");
        detikConvertValue.setText(detik + "");
    }

    private double calculateSecond(int input, String type) {
        double res = 0;
        if(type.equals(DETIK)) {
            res = input;
        } else {
            if(type.equals(JAM)) {
                res = input * 3600;
            } else {
                res = input * 60;
            }
        }
        return res;
    }

    private double calculateHours(double second) {
        return second / 3600;
    }

    private double calculateMinute(double second) {
        return second / 60;
    }

    private double setDoubleFormat(double tes) {
        NumberFormat formatter = new DecimalFormat("##.#####");
        return Double.valueOf(formatter.format(tes));
    }

}
