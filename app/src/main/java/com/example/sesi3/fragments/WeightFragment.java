package com.example.sesi3.fragments;


import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.sesi3.R;

import java.text.DecimalFormat;
import java.text.NumberFormat;


/**
 * A simple {@link Fragment} subclass.
 */
public class WeightFragment extends Fragment {

    private Spinner weightTarget;
    private EditText weightInput;
    private TextView miligramConvertValue;
    private TextView gramConvertValue;
    private TextView poundConvertValue;
    private TextView kilogramConvertValue;

    private final String MILIGRAM = "miligram";
    private final String GRAM = "gram";
    private final String POUND = "pound";
    private final String KILOGRAM = "kilogram";
    private final String BUNDLE_INPUT = "input_state";

    private int destinationWeight;

    public WeightFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_weight, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initComponents(view);
        setValueWeightTarget();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState != null) {
            String inputState = savedInstanceState.getString(BUNDLE_INPUT);
            String miligramState = savedInstanceState.getString(MILIGRAM);
            String kilogramState = savedInstanceState.getString(KILOGRAM);
            String poundState = savedInstanceState.getString(POUND);
            String gramState = savedInstanceState.getString(GRAM);

            weightInput.setText(inputState);
            miligramConvertValue.setText(miligramState);
            kilogramConvertValue.setText(kilogramState);
            poundConvertValue.setText(poundState);
            gramConvertValue.setText(gramState);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(BUNDLE_INPUT, weightInput.getText().toString());
        outState.putString(MILIGRAM, miligramConvertValue.getText().toString());
        outState.putString(KILOGRAM, kilogramConvertValue.getText().toString());
        outState.putString(GRAM, gramConvertValue.getText().toString());
        outState.putString(POUND, poundConvertValue.getText().toString());
    }

    private void setValueWeightTarget() {
        weightTarget.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                destinationWeight = position;
                if(weightInput.length() != 0) {
                    int weight = Integer.parseInt(weightInput.getText().toString());
                    calculateWeight(position, weight);
                } else {
                    resetValue();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initComponents(View view) {
        weightInput = view.findViewById(R.id.input_weight);
        weightTarget = view.findViewById(R.id.weight_target);
        miligramConvertValue = view.findViewById(R.id.miligram_convert_value);
        gramConvertValue = view.findViewById(R.id.gram_convert_value);
        kilogramConvertValue = view.findViewById(R.id.kg_convert_value);
        poundConvertValue = view.findViewById(R.id.lb_convert_value);

        weightInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() == 0) {
                    resetValue();
                } else {
                    int weight = Integer.parseInt(s.toString());
                    calculateWeight(destinationWeight, weight);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void resetValue() {
        double reset = 0;
        miligramConvertValue.setText(reset + "");
        gramConvertValue.setText(reset + "");
        kilogramConvertValue.setText(reset + "");
        poundConvertValue.setText(reset + "");
    }

    @SuppressLint("SetTextI18n")
    private void calculateWeight(int position, int input) {
        double miligram, gram, pound, kilogram;
        switch (position) {
            case 0 :
                gram = setDoubleFormat(calculateGram(input, MILIGRAM));
                break;
            case 1 :
                gram = setDoubleFormat(calculateGram(input, KILOGRAM));
                break;
            case 2 :
                gram = setDoubleFormat(calculateGram(input, GRAM));
                break;
            case 3 :
                gram = setDoubleFormat(calculateGram(input, POUND));
                break;
            default:
                gram = setDoubleFormat(calculateGram(input, MILIGRAM));
                break;
        }

        miligram = setDoubleFormat(calculateMiligram(gram));
        kilogram = setDoubleFormat(calculateKilogram(gram));
        pound = setDoubleFormat(calculatePound(gram));

        Log.e(WeightFragment.class.getSimpleName(), "position: "+position);
        Log.e(WeightFragment.class.getSimpleName(), "calculateWeight: "+kilogram);

        miligramConvertValue.setText(miligram + "");
        kilogramConvertValue.setText(kilogram + "");
        poundConvertValue.setText(pound + "");
        gramConvertValue.setText(gram + "");
    }

    private double calculateGram(int input, String type) {
        double res = 0;
        if(type.equals(GRAM)) {
            res = input;
        } else {
            switch (type) {
                case MILIGRAM :
                    res = input * 0.0001;
                    break;
                case KILOGRAM :
                    res = input * 1000;
                    break;
                case POUND :
                    res = input * 453.592;
                    break;
            }
        }
        return res;
    }

    private double calculateMiligram(double gram) {
        return gram * 1000;
    }

    private double calculateKilogram(double gram) {
        return gram / 1000;
    }

    private double calculatePound(double gram) {
        return gram / 453.592;
    }

    private double setDoubleFormat(double tes) {
        NumberFormat formatter = new DecimalFormat("##.#####");
        return Double.valueOf(formatter.format(tes));
    }
}
